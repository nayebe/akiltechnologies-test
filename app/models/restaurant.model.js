module.exports = mongoose => {
  var schema = mongoose.Schema(
    {
      name: String,
      neighborhood: String,
      photograph: String,
      address: String,
      latlng: Object,
      cuisine_type: String,
      operating_hours: Object,
      deleted: Boolean
    },
    { timestamps: true }
  );

  schema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Restaurant = mongoose.model("restaurant", schema);
  return Restaurant;
};