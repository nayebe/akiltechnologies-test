const db = require("../models");
const Restaurant = db.restaurants;

// Create and Save a new restaurant
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({ message: "Content can nn  not be empty!" });
    return;
  }

  // Create a restaurant
  const restaurant = new Restaurant({
    name: req.body.name,
    neighborhood: req.body.neighborhood,
    photograph: req.body.photograph,
    address: req.body.address,
    latlng: req.body.latlng,
    cuisine_type: req.body.cuisine_type,
    operating_hours: req.body.operating_hours,
    deleted: req.body.deleted ? req.body.deleted : false
  });

  // Save restaurant in the database
  restaurant
    .save(restaurant)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the restaurants."
      });
    });
};

// Retrieve all restaurant from the database.
exports.findAll = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {};

  Restaurant.find({"deleted":false})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

// Find a single restaurants with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Restaurant.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found restaurants with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving restaurants with id=" + id });
    });
};

// Update a restaurants by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Restaurant.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update restaurants with id=${id}. Maybe restaurants was not found!`
        });
      } else res.send({ message: "restaurants was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating restaurants with id=" + id
      });
    });
};


// Update a restaurants by the id in the request
exports.logicalDelete = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Restaurant.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete restaurants with id=${id}. Maybe restaurants was not found!`
        });
      } else res.send({ message: "restaurants was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating restaurants with id=" + id
      });
    });
};

// Delete a restaurants with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Restaurant.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete restaurants with id=${id}. Maybe restaurants was not found!`
        });
      } else {
        res.send({
          message: "restaurants was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete restaurants with id=" + id
      });
    });
};

// Delete all restaurants from the database.
exports.deleteAll = (req, res) => {
  Restaurant.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} restaurants were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all restaurants."
      });
    });
};


