# requierment
git 
npm : 6.13.X or hightler 
node : 14.15.5 or hightler
Docker version 20.10.3, build 48d30b5 or hightler

# instructioon to start the service
# just run this commant
Firstly clone this repository on gitub

$ git clone https://gitlab.com/nayebe/akiltechnologies-test.git

# Run just the deploy.sh to start the api like this in your terminal

./deploy.sh

# If evrything went ok you ready to test the api

url :  localhost:5000/api/restaurants

payload to create a new restaurant

# Methodr POST

{
	"name": "Emily",
    "neighborhood": "Brooklyn",
    "photograph": "2.jpg",
    "address": "919 Fulton St, Brooklyn, NY 11238",
    "latlng": {
      "lat": 40.683555,
      "lng": -73.966393
    },
    "cuisine_type": "Pizza",
    "operating_hours": {
      "Monday": "5:30 pm - 11:00 pm",
      "Tuesday": "5:30 pm - 11:00 pm",
      "Wednesday": "5:30 pm - 11:00 pm",
      "Thursday": "5:30 pm - 11:00 pm",
      "Friday": "5:30 pm - 11:00 pm",
      "Saturday": "5:00 pm - 11:30 pm",
      "Sunday": "12:00 pm - 3:00 pm, 5:00 pm - 11:00 pm"
    }
}


# To retrieve all restaurants 

#Methode GET
url :  localhost:5000/api/restaurants


# For  update or Delete

url :  localhost:5000/api/restaurants/id

#Find restaurants where the name contain the given charatere 

url :  localhost:5000/api/restaurants?name⁼mil






